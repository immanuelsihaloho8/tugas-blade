@extends('layout/main')

@section('title', 'About Me')
@section('container')
<div class="container">
    <h1 class="mt-2">What I Learn</h1>
    <?php 
    $learn =array('Website' =>'HTML & CSS','Database'=>'MySQL','Data Science'=>'Phyton');
    foreach($learn as $name => $type){
        echo '- '. $name. ' : '. $type. '<br />';
    }
    ?>
</div>
@endsection
