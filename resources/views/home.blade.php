@extends('layout/main')

@section('title', 'Hello World!')
@section('container')
<div class="container">
    <h1 class="mt-2">Hello World!</h1>
</div>
@endsection
